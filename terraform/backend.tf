## The backend to use to store the Terraform state
# https://www.terraform.io/language/settings/backends

# Local storage
# https://www.terraform.io/language/settings/backends/local
# terraform {
#   backend "local" {
#   }
# }

# Remote storage using HTTP REST API for GitLab
# https://www.terraform.io/language/settings/backends/http
# https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html
terraform {
  backend "http" {
  }
}
