# Infrastructure demo

These steps will request virtual machines (VMs) on Google Compute Engine (GCE) that will be managed by Terraform and Ansible.

## Request the virtual machines (VMs) with Terraform

_Highly inspired from the following guide: [Get started with Terraform](https://cloud.google.com/docs/terraform/get-started-with-terraform)._

Access the [Google Cloud Console](https://console.cloud.google.com) and create a new Google Cloud project. Save the project ID, it will be used later.

Enable the Compute Engine API: https://console.cloud.google.com/flows/enableapi?apiid=compute.googleapis.com

Generate and download the Service Account Key.

Go to `IAM & Admin` -> `Service Accounts`. Select the `Compute Engine default service account` -> `Keys` and add a new key (JSON format). Save the key in the `credentials` directory at the root level of your workspace under the name `gce-service-account-key.json`.

Generate the private/public SSH keys pair that will be used to access the VM in the `credentials` directory at the root level of your workspace.

```sh
# Create a new pair of SSH keys
ssh-keygen \
    -t ed25519 \
    -f gce-root-ssh-key \
    -q \
    -N "" \
    -C ""
```

Create a `terraform.tfvars` file in the `terraform` directory at the root level of your workspace containing the following content (edit where needed):

```sh
gce_root_ssh_pub_key_file_path    = "../credentials/gce-root-ssh-key.pub"
gce_root_username                 = "CHANGE_ME"
gce_service_account_key_file_path = "../credentials/gce-service-account-key.json"
gcp_project_id                    = "CHANGE_ME"
```
The arborescence should look like this:

```
.
├── credentials
│   ├── gce-root-ssh-key
│   ├── gce-root-ssh-key.pub
│   └── labgce-service-account-key.json
└── terraform
    ├── backend.tf
    ├── main.tf
    ├── outputs.tf
    ├── terraform.tfvars
    └── variables.tf
```

_The following commands are run in the `terraform` directory._

Initiliaze the Terraform state.

```sh
# Initialize the Terraform state
terraform init
```

Validate the Terraform state.

```sh
# Check the Terraform configuration
terraform validate
```

Create an execution plan to preview the changes that will be made to the infrastructure and save it locally.

```sh
# Create the execution plan
terraform plan -input=false -out=.terraform/plan.cache
```

If satisfied with the execution plan, apply it

```sh
# Apply the execution plan
terraform apply -input=false .terraform/plan.cache
```

If no errors occur, you have successfully managed to request a VM on Google Cloud using Terraform. You should see the IPs of the Google Compute instances in the console.











Transform the content of the service account key into base64 and save the output of the command, it will be used later.

```sh
# Transform the service account key to base64
base64 gce-service-account-key.json
```

Transform the content of the private/public SSH key pair into base64 and save the output of the commands, it will be used later.

```sh
# Transform the private SSH key to base64
base64 gce-root-ssh-key

# Transform the public SSH key to base64
base64 gce-root-ssh-key.pub
```

Uncomment the `Remote storage using HTTP REST API for GitLab` section of the `backend.tf` file and comment the `Local storage`.

[Add the environment variables to GitLab CI](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) (mask and protect all variables for security reasons):

- `GCE_ROOT_SSH_PRIVATE_KEY`: The content in base64 of the `gce-root-ssh-key` file
- `GCE_ROOT_SSH_PUBLIC_KEY`: The content in base64 of the `gce-root-ssh-key.pub` file
- `GCE_ROOT_USERNAME`: The root username of the instance
- `GCE_SERVICE_ACCOUNT_KEY`: The content in base64 of the `gce-service-account-key.json` file
- `GCP_PROJECT_ID`: The ID of the Google Cloud project
